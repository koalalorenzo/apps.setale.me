resource "azurerm_virtual_machine" "master" {
  name                          = "master"
  location                      = "${azurerm_resource_group.default.location}"
  resource_group_name           = "${azurerm_resource_group.default.name}"
  network_interface_ids         = ["${azurerm_network_interface.master.id}"]
  vm_size                       = "${var.k8s_vm_type}"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "17.04-DAILY"
    version   = "latest"
  }

  storage_os_disk {
    name          = "master-disk"
    vhd_uri       = "${azurerm_storage_account.vms.primary_blob_endpoint}vhds/master.vhd"
    caching       = "ReadWrite"
    create_option = "FromImage"
    disk_size_gb  = "256"
  }

  os_profile {
    computer_name  = "master"
    admin_username = "koalalorenzo"
    admin_password = "${var.master_admin_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
      path     = "/home/koalalorenzo/.ssh/authorized_keys"
      key_data = "${file("~/.ssh/id_rsa.pub")}"
    }
  }
}
