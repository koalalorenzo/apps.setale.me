resource "azurerm_virtual_machine" "workers" {
  count                         = "${var.k8s_workers_count}"
  name                          = "worker${count.index}"
  location                      = "${azurerm_resource_group.default.location}"
  resource_group_name           = "${azurerm_resource_group.default.name}"
  network_interface_ids         = ["${element(azurerm_network_interface.workers.*.id, count.index)}"]
  vm_size                       = "${var.k8s_vm_type}"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "17.04-DAILY"
    version   = "latest"
  }

  storage_os_disk {
    name          = "${count.index}-worker-disk"
    vhd_uri       = "${azurerm_storage_account.vms.primary_blob_endpoint}vhds/worker${count.index}.vhd"
    caching       = "ReadWrite"
    create_option = "FromImage"
  }

  os_profile {
    computer_name  = "worker${count.index}"
    admin_username = "koalalorenzo"
    admin_password = "${var.master_admin_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false

    ssh_keys {
      path     = "/home/koalalorenzo/.ssh/authorized_keys"
      key_data = "${file("~/.ssh/id_rsa.pub")}"
    }
  }
}
