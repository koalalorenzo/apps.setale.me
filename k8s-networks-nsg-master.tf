# This security group is is used by the VMs
resource "azurerm_network_security_group" "master" {
  name                = "apps-secgroup-master"
  location            = "${azurerm_resource_group.default.location}"
  resource_group_name = "${azurerm_resource_group.default.name}"
}

# Outbound

resource "azurerm_network_security_rule" "master-allow-http" {
  depends_on                  = ["azurerm_network_security_group.master"]
  name                        = "out-allow-http"
  priority                    = 100
  direction                   = "Outbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "80"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.default.name}"
  network_security_group_name = "${azurerm_network_security_group.master.name}"
}

resource "azurerm_network_security_rule" "master-allow-https" {
  depends_on                  = ["azurerm_network_security_group.master"]
  name                        = "out-allow-https"
  priority                    = 101
  direction                   = "Outbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "443"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.default.name}"
  network_security_group_name = "${azurerm_network_security_group.master.name}"
}

# Inblound

resource "azurerm_network_security_rule" "master-allow-in-http" {
  depends_on                  = ["azurerm_network_security_group.master"]
  name                        = "in-allow-http"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "80"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.default.name}"
  network_security_group_name = "${azurerm_network_security_group.master.name}"
}

resource "azurerm_network_security_rule" "master-allow-in-https" {
  depends_on                  = ["azurerm_network_security_group.master"]
  name                        = "in-allow-https"
  priority                    = 101
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "443"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.default.name}"
  network_security_group_name = "${azurerm_network_security_group.master.name}"
}

resource "azurerm_network_security_rule" "master-allow-in-containers" {
  depends_on                  = ["azurerm_network_security_group.master"]
  name                        = "in-allow-containers"
  priority                    = 200
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "30000-32767"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.default.name}"
  network_security_group_name = "${azurerm_network_security_group.master.name}"
}

# Comment this to disable SSH
resource "azurerm_network_security_rule" "master-allow-in-ssh" {
  depends_on                  = ["azurerm_network_security_group.master"]
  name                        = "in-allow-ssh"
  priority                    = 300
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "22"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.default.name}"
  network_security_group_name = "${azurerm_network_security_group.master.name}"
}

# Comment this to disable remote API access
resource "azurerm_network_security_rule" "master-allow-in-k8s-api" {
  depends_on                  = ["azurerm_network_security_group.master"]
  name                        = "in-allow-k8s-api"
  priority                    = 301
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "6443"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.default.name}"
  network_security_group_name = "${azurerm_network_security_group.master.name}"
}
