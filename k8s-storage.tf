resource "azurerm_storage_account" "vms" {
  name                   = "appsvmstorage"                              # Also used by k8s
  resource_group_name    = "${azurerm_resource_group.default.name}"
  location               = "${azurerm_resource_group.default.location}"
  account_type           = "Standard_LRS"
  enable_blob_encryption = true
}

resource "azurerm_storage_container" "default" {
  name                  = "vhds"
  resource_group_name   = "${azurerm_resource_group.default.name}"
  storage_account_name  = "${azurerm_storage_account.vms.name}"
  container_access_type = "private"
}
