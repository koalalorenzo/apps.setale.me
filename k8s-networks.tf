resource "azurerm_virtual_network" "default" {
  name                = "apps-virtual-network"
  address_space       = ["10.0.0.0/16"]
  location            = "${azurerm_resource_group.default.location}"
  resource_group_name = "${azurerm_resource_group.default.name}"
}

resource "azurerm_subnet" "default" {
  name                 = "apps-subnet"
  resource_group_name  = "${azurerm_resource_group.default.name}"
  virtual_network_name = "${azurerm_virtual_network.default.name}"
  address_prefix       = "10.0.2.0/24"
}

# A public IP used to access the apps master, useful for terraform to set
# everything up and running also via ssh.
resource "azurerm_public_ip" "master" {
  name                         = "apps-ip-master"
  location                     = "${azurerm_resource_group.default.location}"
  resource_group_name          = "${azurerm_resource_group.default.name}"
  public_ip_address_allocation = "static"
}

# everything up and running also via ssh.
resource "azurerm_public_ip" "workers" {
  count = "${var.k8s_workers_count}"

  name                         = "apps-ip-worker${count.index}"
  location                     = "${azurerm_resource_group.default.location}"
  resource_group_name          = "${azurerm_resource_group.default.name}"
  public_ip_address_allocation = "static"
}

# VM apps master network interface
resource "azurerm_network_interface" "master" {
  name                      = "apps-nic-master"
  location                  = "${azurerm_resource_group.default.location}"
  resource_group_name       = "${azurerm_resource_group.default.name}"
  network_security_group_id = "${azurerm_network_security_group.master.id}"

  ip_configuration {
    name                          = "default"
    subnet_id                     = "${azurerm_subnet.default.id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.0.2.42"

    public_ip_address_id = "${azurerm_public_ip.master.id}"
  }
}

resource "azurerm_network_interface" "workers" {
  count = "${var.k8s_workers_count}"

  name                      = "apps-nic-worker${count.index}"
  location                  = "${azurerm_resource_group.default.location}"
  resource_group_name       = "${azurerm_resource_group.default.name}"
  network_security_group_id = "${azurerm_network_security_group.workers.id}"

  ip_configuration {
    name                          = "default"
    subnet_id                     = "${azurerm_subnet.default.id}"
    private_ip_address_allocation = "dynamic"

    //    load_balancer_backend_address_pools_ids = ["${azurerm_lb_backend_address_pool.default.id}"]

    public_ip_address_id = "${element(azurerm_public_ip.workers.*.id, count.index)}"
  }
}
