###
# Main website
###

resource "azurerm_dns_zone" "default" {
  name                = "setale.me"
  resource_group_name = "${azurerm_resource_group.default.name}"

  lifecycle {
    prevent_destroy = true
  }
}

resource "azurerm_dns_a_record" "main" {
  name                = "@"
  zone_name           = "${azurerm_dns_zone.default.name}"
  resource_group_name = "${azurerm_resource_group.default.name}"
  ttl                 = "10800"                                       # 3h
  records             = ["${azurerm_public_ip.workers.*.ip_address}"]

  lifecycle {
    prevent_destroy = true
  }
}

resource "azurerm_dns_a_record" "apps-master" {
  name                = "apps"
  zone_name           = "${azurerm_dns_zone.default.name}"
  resource_group_name = "${azurerm_resource_group.default.name}"
  ttl                 = "300"                                      # 5 min
  records             = ["${azurerm_public_ip.master.ip_address}"]

  lifecycle {
    prevent_destroy = true
  }
}

resource "azurerm_dns_a_record" "who-is-lorenzo" {
  name                = "who.is.lorenzo"
  zone_name           = "${azurerm_dns_zone.default.name}"
  resource_group_name = "${azurerm_resource_group.default.name}"
  ttl                 = "10800"                                       # 3h
  records             = ["${azurerm_public_ip.workers.*.ip_address}"]

  lifecycle {
    prevent_destroy = true
  }
}

resource "azurerm_dns_a_record" "who-is" {
  name                = "who.is"
  zone_name           = "${azurerm_dns_zone.default.name}"
  resource_group_name = "${azurerm_resource_group.default.name}"
  ttl                 = "3600"                                        # 1h
  records             = ["${azurerm_public_ip.workers.*.ip_address}"]

  lifecycle {
    prevent_destroy = true
  }
}

resource "azurerm_dns_a_record" "www" {
  name                = "www"
  zone_name           = "${azurerm_dns_zone.default.name}"
  resource_group_name = "${azurerm_resource_group.default.name}"
  ttl                 = "10800"                                       # 3h
  records             = ["${azurerm_public_ip.workers.*.ip_address}"]

  lifecycle {
    prevent_destroy = true
  }
}

###
# Blog
###

resource "azurerm_dns_a_record" "blog" {
  name                = "blog"
  zone_name           = "${azurerm_dns_zone.default.name}"
  resource_group_name = "${azurerm_resource_group.default.name}"
  ttl                 = "10800"                                       # 3h
  records             = ["${azurerm_public_ip.workers.*.ip_address}"]

  lifecycle {
    prevent_destroy = true
  }
}

###
# IPFS And Others
###

resource "azurerm_dns_a_record" "ipfs-server" {
  name                = "ipfs"
  zone_name           = "${azurerm_dns_zone.default.name}"
  resource_group_name = "${azurerm_resource_group.default.name}"
  ttl                 = "3600"
  records             = ["${azurerm_public_ip.workers.*.ip_address}"]
}

resource "azurerm_dns_a_record" "k8s-panel" {
  name                = "k8s"
  zone_name           = "${azurerm_dns_zone.default.name}"
  resource_group_name = "${azurerm_resource_group.default.name}"
  ttl                 = "300"
  records             = ["${azurerm_public_ip.workers.*.ip_address}"]
}

resource "azurerm_dns_a_record" "owncloud" {
  name                = "cloud"
  zone_name           = "${azurerm_dns_zone.default.name}"
  resource_group_name = "${azurerm_resource_group.default.name}"
  ttl                 = "300"
  records             = ["${azurerm_public_ip.workers.*.ip_address}"]
}

resource "azurerm_dns_txt_record" "IPFScv" {
  name                = "cv"
  zone_name           = "${azurerm_dns_zone.default.name}"
  resource_group_name = "${azurerm_resource_group.default.name}"
  ttl                 = "3600"

  record {
    value = "dnslink=/ipns/QmfFB7ShRaVPEy9Bbr9fu9xG947KCZqhCTw1utBNHBwGK2"
  }
}

resource "azurerm_dns_txt_record" "keybase" {
  name                = "who.is.lorenzo"
  zone_name           = "${azurerm_dns_zone.default.name}"
  resource_group_name = "${azurerm_resource_group.default.name}"
  ttl                 = "3600"

  record {
    value = "keybase-site-verification=ZqJRPB_8T_PiympxlyZInsexLCdxlDPb1uqGLQn6TD4"
  }
}

resource "azurerm_dns_txt_record" "google_site_blog" {
  name                = "blog"
  zone_name           = "${azurerm_dns_zone.default.name}"
  resource_group_name = "${azurerm_resource_group.default.name}"
  ttl                 = "3600"

  record {
    value = "google-site-verification=yQhsoeD4tvBCaVB3XxXx_VVH85tLRXHo5fowZcUSJhE"
  }
}

###
# Mail
###

resource "azurerm_dns_mx_record" "protonmail" {
  name                = "@"
  zone_name           = "${azurerm_dns_zone.default.name}"
  resource_group_name = "${azurerm_resource_group.default.name}"
  ttl                 = "21600"

  record {
    preference = 10
    exchange   = "mail.protonmail.ch."
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "azurerm_dns_txt_record" "main-txt" {
  name                = "@"
  zone_name           = "${azurerm_dns_zone.default.name}"
  resource_group_name = "${azurerm_resource_group.default.name}"
  ttl                 = "3600"

  # Proton mail
  record {
    value = "protonmail-verification=9f3f19c27d1bd228f281b31d0dd4d058eb886a76"
  }

  # Proton mail
  record {
    value = "v=spf1 include:_spf.protonmail.ch mx ~all"
  }

  # IPFS
  record {
    value = "dnslink=/ipns/QmfBFDynzwg9gypuhEQWFSGnJU2vfqS5989vHqrUKAeK2L"
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "azurerm_dns_txt_record" "protonmail-dkim" {
  name                = "protonmail._domainkey"
  zone_name           = "${azurerm_dns_zone.default.name}"
  resource_group_name = "${azurerm_resource_group.default.name}"
  ttl                 = "3600"

  record {
    value = "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC/KosoQkIhikFHmVO1oDX+ZFyWlAatDNDh5ftqd7u66su6imS1ajgRjY21VmK9jJFo7IcCb/PJOZQB+nVBRJDddDZPxs5ws8pWrPB42JsMxkwzioMQHLbNJygXlr3lVg2FOx9XmcYU1bJfeI/4cv4UPMTtrBtVk802O33Tm0PmWQIDAQAB"
  }
}

resource "azurerm_dns_txt_record" "protonmail-dmarc" {
  name                = "_dmarc"
  zone_name           = "${azurerm_dns_zone.default.name}"
  resource_group_name = "${azurerm_resource_group.default.name}"
  ttl                 = "3600"

  record {
    value = "v=DMARC1; p=none;"
  }
}
