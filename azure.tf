# Generic Things related to the Azure go here (ex: Resource group, providers)

# Microsoft Azure Provider
provider "azurerm" {}

# Create a resource group
resource "azurerm_resource_group" "default" {
  name     = "apps.setale.me"
  location = "westeurope"
}
