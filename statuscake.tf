provider "statuscake" {}

resource "statuscake_test" "blog" {
  website_name  = "blog.setale.me"
  website_url   = "https://blog.setale.me"
  test_type     = "HTTP"
  check_rate    = 300
  confirmations = 2
  contact_id    = 52664
  paused        = true
}

resource "statuscake_test" "website" {
  website_name  = "www.setale.me"
  website_url   = "https://www.setale.me"
  test_type     = "HTTP"
  check_rate    = 300
  confirmations = 2
  contact_id    = 52664
  paused        = true
}

resource "statuscake_test" "profile" {
  website_name  = "who.is.lorenzo.setale.me"
  website_url   = "https://who.is.lorenzo.setale.me"
  test_type     = "HTTP"
  check_rate    = 300
  confirmations = 2
  contact_id    = 52664
  paused        = true
}

resource "statuscake_test" "k8s_api" {
  website_name  = "apps.setale.me"
  website_url   = "https://apps.setale.me"
  test_type     = "HTTP"
  port          = 6443
  check_rate    = 300
  confirmations = 2
  contact_id    = 52664
  paused        = true
}

resource "statuscake_test" "ipfs" {
  website_name  = "siderus.io"
  website_url   = "http://siderus.io/ipns/ipfs.io/"
  test_type     = "HTTP"
  check_rate    = 3600
  confirmations = 2
  contact_id    = 52664
  paused        = false
}
