####
## Please, please use TF environment variables!! check `Readme.md`
####

variable "master_admin_password" {
  # To generate it use:
  # $ export TF_VAR_master_admin_password="$(openssl rand -base64 48)""
  description = "Master VM admin password (please use variables!!!)"
}

variable "k8s_vm_type" {
  description = "The class/type to use when creating VMs (default: Basic_A1)"
  default     = "Basic_A1"
}

variable "k8s_workers_count" {
  description = "The amount of workers required (default: 1)"
  default     = 3
}

variable "k8s_token" {
  description = "K8s token used for administration (used if workers are required)"
}
