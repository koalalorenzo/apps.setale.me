output "K8S Master IP" {
  value = "${azurerm_public_ip.master.ip_address}"
}

output "K8S Workers IPs" {
  value = "${azurerm_public_ip.workers.*.ip_address}"
}

data "template_file" "ansible_inventory" {
  template = "${file("terraform_templates/inventory.tpl")}"

  vars {
    master_priv_ip_address = "${azurerm_network_interface.master.private_ip_address}"
    master_pub_ip_address  = "${azurerm_public_ip.master.ip_address}"
    workers_ips            = "${join("\n",azurerm_network_interface.workers.*.private_ip_address)}"
    admin_username         = "koalalorenzo"
    admin_password         = "${var.master_admin_password}"
    k8s_token              = "${var.k8s_token}"
  }
}

# Render the template locally
resource "null_resource" "local" {
  triggers {
    template = "${data.template_file.ansible_inventory.rendered}"
  }

  provisioner "local-exec" {
    command = "echo \"${data.template_file.ansible_inventory.rendered}\" > ./inventory"
  }
}
