# This security group is is used by the VMs
resource "azurerm_network_security_group" "workers" {
  name                = "apps-secgroup-workers"
  location            = "${azurerm_resource_group.default.location}"
  resource_group_name = "${azurerm_resource_group.default.name}"
}

# Outbound

resource "azurerm_network_security_rule" "workers-allow-http" {
  depends_on                  = ["azurerm_network_security_group.workers"]
  name                        = "out-allow-http"
  priority                    = 100
  direction                   = "Outbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "80"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.default.name}"
  network_security_group_name = "${azurerm_network_security_group.workers.name}"
}

resource "azurerm_network_security_rule" "workers-allow-https" {
  depends_on                  = ["azurerm_network_security_group.workers"]
  name                        = "out-allow-https"
  priority                    = 101
  direction                   = "Outbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "443"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.default.name}"
  network_security_group_name = "${azurerm_network_security_group.workers.name}"
}

# Inblound

resource "azurerm_network_security_rule" "workers-allow-in-http" {
  depends_on                  = ["azurerm_network_security_group.workers"]
  name                        = "in-allow-http"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "80"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.default.name}"
  network_security_group_name = "${azurerm_network_security_group.workers.name}"
}

resource "azurerm_network_security_rule" "workers-allow-in-https" {
  depends_on                  = ["azurerm_network_security_group.workers"]
  name                        = "in-allow-https"
  priority                    = 101
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "443"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.default.name}"
  network_security_group_name = "${azurerm_network_security_group.workers.name}"
}

resource "azurerm_network_security_rule" "workers-allow-in-containers" {
  depends_on                  = ["azurerm_network_security_group.workers"]
  name                        = "in-allow-containers"
  priority                    = 200
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "*"
  source_port_range           = "*"
  destination_port_range      = "30000-32767"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.default.name}"
  network_security_group_name = "${azurerm_network_security_group.workers.name}"
}
