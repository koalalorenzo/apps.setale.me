[masters]
${master_priv_ip_address}

[workers]
${workers_ips}

[all:vars]
k8s_token='${k8s_token}'
master_ip_address='${master_priv_ip_address}'
master_public_address='${master_pub_ip_address}'
ansible_user='${admin_username}'
ansible_become_pass='${admin_password}'
ansible_ssh_common_args='-o ProxyCommand=\"ssh -W %h:%p ${admin_username}@${master_pub_ip_address}\"'